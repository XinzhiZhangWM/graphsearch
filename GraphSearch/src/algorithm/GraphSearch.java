package algorithm;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;


public class GraphSearch {
	static class Graph{
		int n;
		double p;
		LinkedList<Integer>[] vertices;
		Graph(int n, double p){
			this.n = n;
			this.p = p;
			vertices = new LinkedList[n];
			for (int i = 0; i < n; i++) {
				vertices[i] = new LinkedList<>();
			}
		}
	}
	static void addEdge(Graph graph) {
		for (int i = 0; i < (graph.n - 1); i++) {
			int j = i + 1;
			while (j < graph.n) {
				double randNum = Math.random();
				if (randNum < graph.p) {
					graph.vertices[i].add(j);
					graph.vertices[j].add(i);
				}
				j++;
			}
		}
	}
	static int findMaxConnectedComponent(Graph graph, int t) {
		int result = 0;
		for(int i = 0; i < graph.n; i++) {
			Queue<Integer> queue = new LinkedList<Integer>();
			HashSet<Integer> visited = new HashSet<Integer>();
			visited.add(i);
			queue.add(i);
			while (!queue.isEmpty()) {
				int curNode = queue.remove();
				LinkedList<Integer> adjacentNodes = graph.vertices[curNode];
				for (int vertex : adjacentNodes) {
					if (!visited.contains(vertex)) {
						queue.add(vertex);
						visited.add(vertex);
					}
				}
			}
			if (visited.size() >= t) {
				result = 1;
			}
		}
		return result;
	}
	public static void main(String[] args) {
		int n = 40;
		double p;
		double c = 0.2;
		int t = 30;
		int num;
		int total;
		while (c <= 3.0) {
			System.out.println("------");
			System.out.println(c);
			p = c/n;
			num = 0;
			total = 0;
			while (num < 500) {
				Graph graph = new Graph(n, p);
				addEdge(graph);
				int result = findMaxConnectedComponent(graph, t);
				if (result == 1) {
					total += 1;
				}
				num ++;
			}
			
			double avg = (total/500.0)*100;
			//Round the average percentage with 1 decimal
			BigDecimal bd = BigDecimal.valueOf(avg);
		    bd = bd.setScale(1, RoundingMode.HALF_UP);
			avg = bd.doubleValue();
			System.out.println(avg);
			//Round the c value with 1 decimal
			c += 0.2;
			bd = BigDecimal.valueOf(c);
		    bd = bd.setScale(1, RoundingMode.HALF_UP);
			c = bd.doubleValue();
		}
	}
}
